$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    //Eventos de modal
    $('#contacto').on('show.bs.modal', function (e) {
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-default');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        //alert('El model finalizo');
        $('#contactoBtn').prop('disabled', false);
    });
});